package Source;

import java.util.*;
import java.util.function.UnaryOperator;

public class CLinkedList<T> implements List<T> {
    private static class NodeElement<T> {
        T item;
        NodeElement<T> next;
        NodeElement<T> previos;

        NodeElement(T item) {
            this.item = item;
        }
    }

    private class CIterator implements Iterator<T> {
        private NodeElement<T> currentNode;

        CIterator(CLinkedList<T> list) {
            currentNode = list.start;
        }

        @Override
        public boolean hasNext() {
            return currentNode != null;
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T item = currentNode.item;
            currentNode = currentNode.next;
            return item;
        }

        @Override
        public void remove() {
            if (currentNode != null) {
                NodeElement<T> oldPrevios = currentNode.previos.previos;
                NodeElement<T> oldNext = currentNode.previos.next;
                if (currentNode.previos.previos == null) {
                    start = currentNode.previos.next;
                    start.previos = null;
                } else {
                    currentNode.previos.previos.next = oldNext;
                    currentNode.previos.next.previos = oldPrevios;
                }
            } else {
                if (cursor.previos == null && cursor.next == null) {
                    clear();
                    size = 0;
                    return;
                } else {
                    cursor.item = cursor.previos.item;
                    cursor.previos.next = null;
                }

            }

            size--;
        }

    }

    private class CListIterator implements ListIterator<T> {
        private final NodeElement<T> start;
        private final NodeElement<T> end;
        private NodeElement<T> currentNode;
        private int previosFlag = 0;
        private int flagForRemove = 0;
        private int lastCountRept = 0;

        CListIterator(CLinkedList<T> list) {
            start = list.start;
            end = list.cursor;
            currentNode = start;
        }

        @Override
        public boolean hasNext() {
            return currentNode != null;
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            flagForRemove = 0;
            T item = currentNode.item;
            currentNode = currentNode.next;
            return item;
        }

        @Override
        public boolean hasPrevious() {
            if (currentNode == null) {
                currentNode = end;
                previosFlag = 1;
            }
            return currentNode.previos != null;
        }

        @Override
        public T previous() {
            if (!hasPrevious()) throw new NoSuchElementException();
            if (previosFlag != 0) {
                previosFlag = 0;
                return currentNode.item;
            }
            flagForRemove = 1;
            currentNode = currentNode.previos;
            return currentNode.item;
        }

        @Override
        public int nextIndex() {
            Object elementToFound;
            if (currentNode == null) {
                elementToFound = cursor.item;
            } else {
                if (currentNode.previos == null) return 0;
                elementToFound = currentNode.previos.item;
            }
            NodeElement<T> start = this.start;
            int index = 1;
            while (start.next != null) {
                if (start.item == elementToFound) {
                    break;
                }
                index++;
                start = start.next;
            }
            return index;
        }

        @Override
        public int previousIndex() {
            Object elementToFound;
            if (currentNode == null) {
                elementToFound = cursor.item;
            } else {
                if (currentNode.previos == null) return -1;
                elementToFound = currentNode.previos.item;
            }
            NodeElement<T> start = this.start;
            int index = 0;
            while (start.next != null) {
                if (start.item == elementToFound) {
                    break;
                }
                index++;
                start = start.next;
            }
            return index;
        }

        private void removeElement(NodeElement<T> removeElement) {
            removeElement.next.previos = removeElement.previos;
            removeElement.previos.next = removeElement.next;
        }

        private void replaceElements() {
            NodeElement<T> localStart = this.start;
            while (localStart.next != null) {
                localStart.item = localStart.next.item;
                localStart = localStart.next;
            }
            localStart.previos.next = null;
        }

        private void replaceElementUpgrade() {
            NodeElement<T> localCursor = this.start;
            NodeElement<T> localStart = this.start;
            while (localCursor.next != null) {
                localStart.item = localCursor.next.item;
                localStart.next = localCursor.next.next;
                if (localStart.next != null) localCursor.next.previos = localStart;
                if (localStart.item == currentNode.item) break;
                localCursor = localCursor.next;
            }
        }

        @Override
        public void remove() {
            NodeElement<T> nodeToRemove;
            if (currentNode == null) {
                nodeToRemove = end;
            } else if (currentNode.previos == null) {
                nodeToRemove = start;
            } else {
                nodeToRemove = (flagForRemove == 0) ? currentNode.previos : currentNode;
            }
            if (nodeToRemove.previos != null && nodeToRemove.next != null) {
                removeElement(nodeToRemove);
            }

            if (nodeToRemove.previos == null && nodeToRemove.next != null) {
                if (flagForRemove == 0) {
                    replaceElementUpgrade();
                } else {
                    replaceElements();
                }
            }
            if (nodeToRemove.next == null && nodeToRemove.previos != null) {
                nodeToRemove.previos.next = null;
            }
            if (currentNode != null && currentNode.item == end.item) lastCountRept++;
            if (size == 1 || lastCountRept == 2) {
                clear();
            } else {
                size--;
            }

        }

        @Override
        public void set(T o) {
            NodeElement<T> setNode = (currentNode != null) ? currentNode.previos : end;
            setNode.item = o;
        }

        @Override
        public void add(T o) {
            NodeElement<T> nodeToAdd = new NodeElement<>(o);
            NodeElement<T> currentNodeLocal;
            if (currentNode != null) {
                currentNodeLocal = currentNode.previos;
                nodeToAdd.previos = currentNodeLocal;
                nodeToAdd.next = currentNode;
                currentNodeLocal.next = nodeToAdd;
                currentNode.previos = nodeToAdd;
            } else {
                nodeToAdd.next = null;
                end.next = nodeToAdd;
                nodeToAdd.previos = end;
            }
        }
    }

    private NodeElement<T> start, cursor = null;
    private int size = 0;

    @Override
    public boolean add(T o) {
        NodeElement<T> addElement = new NodeElement<>(o);
        if (isEmpty()) {
            start = cursor = addElement;
            start.previos = null;
        } else {
            cursor.next = addElement;
            addElement.previos = cursor;
            cursor = addElement;
        }
        cursor.next = null;
        size++;
        return true;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return start == null;
    }

    @Override
    public boolean contains(Object element) {
        if (isEmpty()) return false;
        Iterator<T> iter = this.iterator();
        while (iter.hasNext()) if (iter.next() == element) return true;
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new CIterator(this);
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        Iterator<T> iter = this.iterator();
        while (iter.hasNext()) {
            if (iter.next() == o) {
                iter.remove();
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        if (c.size() == 0) return false;
        c.forEach(element -> add((T) element));
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceAll(UnaryOperator operator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sort(Comparator c) {
//        Was using booble sort (speed can to be very slowly because camon it`s booble sort)
        int countOfMax = 0;
        for (int i = 0; i < size - countOfMax; i++) {
            NodeElement<T> showCursor = start;
            while (showCursor != null) {
                if (showCursor.next == null) break;
                if (c.compare(showCursor.item, showCursor.next.item) > 0) {
                    T temp = showCursor.next.item;
                    showCursor.next.item = showCursor.item;
                    showCursor.item = temp;
                }
                showCursor = showCursor.next;
            }
            countOfMax++;
        }

    }

    @Override
    public void clear() {
        start = null;
        cursor = null;
        size = 0;
    }

    private NodeElement<T> getNode(int index) {
        if (index >= size) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        NodeElement<T> showCursor = start;
        while (index != 0) {
            showCursor = showCursor.next;
            index--;
        }
        return showCursor;
    }

    @Override
    public T get(int index) {
        return getNode(index).item;
    }

    @Override
    public T set(int index, T element) {
        NodeElement<T> setNode = getNode(index);
        T oldValues = setNode.item;
        setNode.item = element;
        return oldValues;
    }

    @Override
    public void add(int index, T element) {
        // get old node elemens and make new nodeelement
        NodeElement<T> currentNode = getNode(index);
        NodeElement<T> oldPrevios = currentNode.previos;
        // Set dependecy for elements
        if (oldPrevios != null) {
            NodeElement<T> elementToAdd = new NodeElement<>(element);
            elementToAdd.next = currentNode;
            oldPrevios.next = elementToAdd;
        } else {
            NodeElement<T> copyCurrent = new NodeElement<>(currentNode.item);
            copyCurrent.next = currentNode.next;
            copyCurrent.previos = currentNode;
            currentNode.next = copyCurrent;
            currentNode.item = element;
        }

    }

    @Override
    public T remove(int index) {
        if (index >= size) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        Iterator<T> iter = this.iterator();
        int numbInter = 0;
        T removedElement = null;
        while (iter.hasNext()) {
            removedElement = iter.next();
            if (numbInter == index) {
                iter.remove();
                size--;
                break;
            }
            numbInter++;
        }
        return removedElement;
    }

    @Override
    public int indexOf(Object o) {
        if (!contains(o)) return 0;
        int indexOfElement = 0;
        for (T t : this) {
            if (t == o) break;
            indexOfElement++;
        }
        return indexOfElement;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (isEmpty()) return 0;
        int indexOfLast = size - 1;
        NodeElement<T> showCursor = cursor;
        while (showCursor != null) {
            if (showCursor.item == o || indexOfLast == 0) break;
            indexOfLast--;
            showCursor = showCursor.previos;
        }
        return indexOfLast;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new CListIterator(this);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        start = getNode(index);
        return new CListIterator(this);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Spliterator<T> spliterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException();
    }
}