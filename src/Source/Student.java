package Source;


import java.time.LocalDate;

public class Student {
    private int uniqId = 0;
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    public Student(String name, LocalDate dateOfBirth, String details,int uniqId) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
        this.uniqId = uniqId;
    }

    public int getUniqId() {
        return uniqId;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDetails() {
        return details;
    }


}
